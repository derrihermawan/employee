package com.office.employee.dtomodels;

import java.util.Date;


public class LemburdanbonusDTO {
	private Long idLemburdanbonus;
	private KaryawanDTO karyawan;
	private int variableBonus;
	private int jumlahLembur;
	private Date tanggal;
	
	public LemburdanbonusDTO() {
		// TODO Auto-generated constructor stub
	}

	public LemburdanbonusDTO(Long idLemburdanbonus, KaryawanDTO karyawan, int variableBonus, int jumlahLembur,
			Date tanggal) {
		super();
		this.idLemburdanbonus = idLemburdanbonus;
		this.karyawan = karyawan;
		this.variableBonus = variableBonus;
		this.jumlahLembur = jumlahLembur;
		this.tanggal = tanggal;
	}

	public Long getIdLemburdanbonus() {
		return idLemburdanbonus;
	}

	public void setIdLemburdanbonus(Long idLemburdanbonus) {
		this.idLemburdanbonus = idLemburdanbonus;
	}

	public KaryawanDTO getKaryawan() {
		return karyawan;
	}

	public void setKaryawan(KaryawanDTO karyawan) {
		this.karyawan = karyawan;
	}

	public int getVariableBonus() {
		return variableBonus;
	}

	public void setVariableBonus(int variableBonus) {
		this.variableBonus = variableBonus;
	}

	public int getJumlahLembur() {
		return jumlahLembur;
	}

	public void setJumlahLembur(int jumlahLembur) {
		this.jumlahLembur = jumlahLembur;
	}

	public Date getTanggal() {
		return tanggal;
	}

	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}
	
}
