package com.office.employee.dtomodels;

public class UserIdDTO {
	 private Long idUser;
     private String username;
     
     public UserIdDTO() {
		// TODO Auto-generated constructor stub
	}

	public UserIdDTO(Long idUser, String username) {
		super();
		this.idUser = idUser;
		this.username = username;
	}

	public Long getIdUser() {
		return idUser;
	}

	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
     
}
