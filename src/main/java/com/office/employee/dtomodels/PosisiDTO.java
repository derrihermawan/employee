package com.office.employee.dtomodels;

public class PosisiDTO {
	private Long idPosisi;
    private String namaPosisi;
	public PosisiDTO(Long idPosisi, String namaPosisi) {
		super();
		this.idPosisi = idPosisi;
		this.namaPosisi = namaPosisi;
	}
    
    public PosisiDTO() {
		// TODO Auto-generated constructor stub
	}

	public Long getIdPosisi() {
		return idPosisi;
	}

	public void setIdPosisi(Long idPosisi) {
		this.idPosisi = idPosisi;
	}

	public String getNamaPosisi() {
		return namaPosisi;
	}

	public void setNamaPosisi(String namaPosisi) {
		this.namaPosisi = namaPosisi;
	}
	
	
    
}
