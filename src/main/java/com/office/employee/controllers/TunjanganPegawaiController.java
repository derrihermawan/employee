package com.office.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.office.employee.dtomodels.TunjanganPegawaiDTO;
import com.office.employee.models.TunjanganPegawai;
import com.office.employee.repositories.TunjanganPegawaiRepository;

@RestController
@RequestMapping("api/tunjanganpegawai")
public class TunjanganPegawaiController {
	@Autowired
	TunjanganPegawaiRepository tunjanganPegawaiRepository;
	ModelMapper modelMapper = new ModelMapper();
	
	@PostMapping("/create")
	public Map<String, Object> inputDataTunjanganPegawai(@Valid @RequestBody TunjanganPegawaiDTO tunjanganPegawaiDTO){
		TunjanganPegawai tunjanganPegawai = modelMapper.map(tunjanganPegawaiDTO, TunjanganPegawai.class);
		tunjanganPegawaiRepository.save(tunjanganPegawai);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Create data tunjanganPegawai succesful");
		result.put("Data", tunjanganPegawai);
		return result;
	}
	
	@GetMapping("/readAll")
	public Map<String, Object> getAllTunjanganPegawai (){
		List<TunjanganPegawai> listTunjanganPegawai = tunjanganPegawaiRepository.findAll();
		List<TunjanganPegawaiDTO> listTunjanganPegawaiDTO = new ArrayList<TunjanganPegawaiDTO>();
		for(TunjanganPegawai tunjanganPegawai : listTunjanganPegawai) {
			TunjanganPegawaiDTO tunjanganPegawaiDTO = modelMapper.map(tunjanganPegawai, TunjanganPegawaiDTO.class);
			listTunjanganPegawaiDTO.add(tunjanganPegawaiDTO);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Read all data tunjanganPegawai succesful");
		result.put("Data", listTunjanganPegawaiDTO);
		return result;
	}
	
	@GetMapping("/read/{id}")
	public Map<String, Object> getTunjanganPegawaiById(@PathVariable(value = "id")Long idTunjanganPegawai){
		TunjanganPegawai tunjanganPegawai = tunjanganPegawaiRepository.findById(idTunjanganPegawai).get();
		TunjanganPegawaiDTO tunjanganPegawaiDTO = modelMapper.map(tunjanganPegawai, TunjanganPegawaiDTO.class);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Read data tunjanganPegawai by id succesful");
		result.put("Data", tunjanganPegawaiDTO);
		return result;
	}
	
	@PutMapping("/update/{id}")
	public Map<String, Object> updateDataTunjanganPegawai(@PathVariable(value = "id") Long idTunjanganPegawai, @Valid @RequestBody TunjanganPegawaiDTO tunjanganPegawaiDTO){
		TunjanganPegawai tunjanganPegawai = modelMapper.map(tunjanganPegawaiDTO, TunjanganPegawai.class);
		tunjanganPegawai.setIdTunjanganPegawai(idTunjanganPegawai);
		tunjanganPegawaiRepository.save(tunjanganPegawai);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Update data tunjanganPegawai succesful");
		result.put("Data", tunjanganPegawai);
		return result;
	}
	
	@DeleteMapping("/delete/{id}")
	public Map<String, Object> deleteDataTunjanganPegawai(@PathVariable(value = "id")Long idTunjanganPegawai){
		TunjanganPegawai tunjanganPegawai = tunjanganPegawaiRepository.findById(idTunjanganPegawai).get();
		tunjanganPegawaiRepository.delete(tunjanganPegawai);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Delete data tunjanganPegawai succesful");
		result.put("Data", ResponseEntity.ok().build());
		return result;
	}
}
