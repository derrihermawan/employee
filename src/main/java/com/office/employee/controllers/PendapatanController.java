package com.office.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.office.employee.dtomodels.PendapatanDTO;
import com.office.employee.models.Pendapatan;
import com.office.employee.repositories.PendapatanRepository;

@RestController
@RequestMapping("/api/pendapatan")
public class PendapatanController {
	@Autowired 
	PendapatanRepository pendapatanRepository;
	ModelMapper modelMapper = new ModelMapper();
	
	@PostMapping("/create")
	public Map<String, Object> inputDataPendapatan(@Valid @RequestBody PendapatanDTO pendapatanDTO){
		Pendapatan pendapatan = modelMapper.map(pendapatanDTO, Pendapatan.class);
		pendapatanRepository.save(pendapatan);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Create data pendapatan succesful");
		result.put("Data", pendapatan);
		return result;
	}
	
	@GetMapping("/readAll")
	public Map<String, Object> getAllPendapatan (){
		List<Pendapatan> listPendapatan = pendapatanRepository.findAll();
		List<PendapatanDTO> listPendapatanDTO = new ArrayList<PendapatanDTO>();
		for(Pendapatan pendapatan : listPendapatan) {
			PendapatanDTO pendapatanDTO = modelMapper.map(pendapatan, PendapatanDTO.class);
			listPendapatanDTO.add(pendapatanDTO);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Read all data pendapatan succesful");
		result.put("Data", listPendapatanDTO);
		return result;
	}
	
	@GetMapping("/read/{id}")
	public Map<String, Object> getPendapatanById(@PathVariable(value = "id")Long idPendapatan){
		Pendapatan pendapatan = pendapatanRepository.findById(idPendapatan).get();
		PendapatanDTO pendapatanDTO = modelMapper.map(pendapatan, PendapatanDTO.class);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Read data pendapatan by id succesful");
		result.put("Data", pendapatanDTO);
		return result;
	}
	
	@PutMapping("/update/{id}")
	public Map<String, Object> updateDataPendapatan(@PathVariable(value = "id") Long idPendapatan, @Valid @RequestBody PendapatanDTO pendapatanDTO){
		Pendapatan pendapatan = modelMapper.map(pendapatanDTO, Pendapatan.class);
		pendapatan.setIdPendapatan(idPendapatan);
		pendapatanRepository.save(pendapatan);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Update data pendapatan succesful");
		result.put("Data", pendapatan);
		return result;
	}
	
	@DeleteMapping("/delete/{id}")
	public Map<String, Object> deleteDataPendapatan(@PathVariable(value = "id")Long idPendapatan){
		Pendapatan pendapatan = pendapatanRepository.findById(idPendapatan).get();
		pendapatanRepository.delete(pendapatan);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Delete data pendapatan succesful");
		result.put("Data", ResponseEntity.ok().build());
		return result;
	}
}
