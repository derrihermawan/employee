package com.office.employee.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.office.employee.dtomodels.KaryawanDTO;
import com.office.employee.dtomodels.LemburdanbonusDTO;
import com.office.employee.dtomodels.ParameterDTO;
import com.office.employee.dtomodels.PendapatanDTO;
import com.office.employee.dtomodels.PosisiDTO;
import com.office.employee.dtomodels.PresentaseGajiDTO;
import com.office.employee.dtomodels.TunjanganPegawaiDTO;
import com.office.employee.models.Karyawan;
import com.office.employee.models.Lemburdanbonus;
import com.office.employee.models.Parameter;
import com.office.employee.models.Pendapatan;
import com.office.employee.models.Posisi;
import com.office.employee.models.PresentaseGaji;
import com.office.employee.models.TunjanganPegawai;
import com.office.employee.repositories.KaryawanRepository;
import com.office.employee.repositories.LemburDanBonusRepository;
import com.office.employee.repositories.ParameterRepository;
import com.office.employee.repositories.PendapatanRepository;
import com.office.employee.repositories.PosisiRepository;
import com.office.employee.repositories.PresentaseGajiRepository;
import com.office.employee.repositories.TunjanganPegawaiRepository;

@RestController
@RequestMapping("/api/employee")
public class SalaryCalculationController {
	@Autowired 
	PendapatanRepository pendapatanRepository;
	
	@Autowired 
	KaryawanRepository karyawanRepository;
	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	PresentaseGajiRepository presentaseGajiRepository;
	
	@Autowired
	TunjanganPegawaiRepository tunjanganPegawaiRepository;
	
	@Autowired
	ParameterRepository parameterRepository;
	
	@Autowired
	LemburDanBonusRepository lemburDanBonusRepository;
	
	@Autowired
	PosisiRepository posisiRepository;
	//api calculation salary for pendapatan
	@PutMapping("/calculationSalary/{date}")
	public Map<String, Object> calculatingSalary(@PathVariable(value = "date")@DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date date){
		Map<String, Object> result = new HashMap<String, Object>();
		List<KaryawanDTO> listKaryawanDTO = mappingKaryawan();
		List<PendapatanDTO> listPendapatanDTO = mappingPendapatan();
		Calendar tanggalGaji = Calendar.getInstance();
		int lamaLembur, variableBonus;
		BigDecimal takeHomePay,pphPerbulan, uangBonus,uangLembur,besaranGaji, gajiPokok, tunjanganKeluarga, tunjanganTransport, tunjanganPegawai, gajiKotor, bpjs, gajiBersih;
		boolean isUpdate;
		//mapping parameter date to format Date
		
		int yearRequest = date.getYear()+1900;
		int monthRequest = date.getMonth()+1;
		
		for(KaryawanDTO karyawanDTO : listKaryawanDTO) {
			//initialization attribute that will use for pendapatanDTO
			besaranGaji = getBesaranGaji(karyawanDTO);
			
			gajiPokok = gajiPokokCalculation(besaranGaji, karyawanDTO);
			tunjanganKeluarga = tunjanganKeluargaCalculation(karyawanDTO.getStatusPernikahan(), gajiPokok);
			tunjanganPegawai = getTunjanganPegawai(karyawanDTO);
			tunjanganTransport = getTunjanganTransport(karyawanDTO);
			gajiKotor = gajiKotorCalculation(gajiPokok, tunjanganKeluarga, tunjanganPegawai, tunjanganTransport);
			bpjs = bpjsCalculation(gajiPokok);
			pphPerbulan = BigDecimal.ZERO;
			gajiBersih = gajiBersihCalculation(gajiKotor, pphPerbulan ,bpjs);
			lamaLembur = getLamaLembur(karyawanDTO);
			variableBonus = getVariableBonus(karyawanDTO);
			isUpdate = false;
			for(PendapatanDTO pendapatanDTO : listPendapatanDTO) {
				
				tanggalGaji.setTime(pendapatanDTO.getTanggalGaji());
				
				Date dateExist = pendapatanDTO.getTanggalGaji();
				int yearExist = dateExist.getYear() + 1900;
				int monthExist = dateExist.getMonth()+1;
				//checking year and month if exist
				if(yearRequest == yearExist && monthRequest == monthExist && karyawanDTO.getIdKaryawan() == pendapatanDTO.getKaryawan().getIdKaryawan()) {
					uangBonus = bonusCalculation(karyawanDTO, dateExist);
					uangLembur = lemburCalculation(karyawanDTO, gajiBersih ,dateExist);
					takeHomePay = takeHomePayCalculation(gajiBersih, uangLembur, uangBonus);
					//if year and month are exist update data to pendapatan
					PendapatanDTO pendapatanDTOwithNewValue = insertPendapatan(gajiPokok, tunjanganKeluarga, tunjanganTransport, tunjanganPegawai, gajiKotor, pphPerbulan, bpjs, gajiBersih, lamaLembur,uangLembur,variableBonus, uangBonus, takeHomePay, dateExist, karyawanDTO);
					Pendapatan pendapatan = modelMapper.map(pendapatanDTOwithNewValue, Pendapatan.class);
					pendapatan.setIdPendapatan(pendapatanDTO.getIdPendapatan());
					pendapatanRepository.save(pendapatan);
					isUpdate = true;
				}
					
			}
			//if year and month are not exist insert data to pendapatan
			if(!isUpdate) {
				uangBonus = bonusCalculation(karyawanDTO, date);
				uangLembur = lemburCalculation(karyawanDTO, gajiBersih ,date);
				takeHomePay = takeHomePayCalculation(gajiBersih, uangLembur, uangBonus);
				PendapatanDTO pendapatanDTO = insertPendapatan(gajiPokok, tunjanganKeluarga, tunjanganTransport, tunjanganPegawai, gajiKotor, pphPerbulan, bpjs, gajiBersih, lamaLembur, uangLembur, variableBonus, uangBonus, takeHomePay, date, karyawanDTO);
				Pendapatan pendapatan = modelMapper.map(pendapatanDTO, Pendapatan.class);
				pendapatanRepository.save(pendapatan);
			}			
		}
		
		result.put("Status", 200);
		result.put("Message", "Calculation Salary succesful");
		
		return result;	
		}
		
	
	
	//filling object pendapatanDTO
	private PendapatanDTO insertPendapatan(BigDecimal gajiPokok,BigDecimal tunjanganKeluarga,BigDecimal tunjanganTransport,BigDecimal tunjanganPegawai,BigDecimal gajiKotor, BigDecimal pphPerbulan,BigDecimal bpjs,BigDecimal gajiBersih, int lamaLembur,BigDecimal uangLembur, int variableBonus, BigDecimal uangBonus, BigDecimal takeHomePay, Date calendar, KaryawanDTO karyawanDTO) {
		PendapatanDTO pendapatanDTO = new PendapatanDTO();
		pendapatanDTO.setGajiPokok(gajiPokok);
		pendapatanDTO.setTunjanganKeluarga(tunjanganKeluarga);
		pendapatanDTO.setTunjanganPegawai(tunjanganPegawai);
		pendapatanDTO.setTunjanganTransport(tunjanganTransport);
		pendapatanDTO.setGajiKotor(gajiKotor);
		pendapatanDTO.setBpjs(bpjs);
		pendapatanDTO.setTanggalGaji(calendar);
		pendapatanDTO.setGajiBersih(gajiBersih);
		pendapatanDTO.setKaryawan(karyawanDTO);
		pendapatanDTO.setLamaLembur(lamaLembur);
		pendapatanDTO.setPphPerbulan(pphPerbulan);
		pendapatanDTO.setTakeHomePay(takeHomePay);
		pendapatanDTO.setUangBonus(uangBonus);
		pendapatanDTO.setUangLembur(uangLembur);
		pendapatanDTO.setVariableBonus(variableBonus);
		
		return pendapatanDTO;
	}
	
	//mapping pendapatan
	private List<PendapatanDTO> mappingPendapatan(){
		List<Pendapatan> listPendapatan = pendapatanRepository.findAll();
		List<PendapatanDTO> listPendapatanDTO = new ArrayList<PendapatanDTO>();
		for(Pendapatan pendapatan : listPendapatan) {
			PendapatanDTO pendapatanDTO = modelMapper.map(pendapatan, PendapatanDTO.class);
			listPendapatanDTO.add(pendapatanDTO);
		}
		return listPendapatanDTO;
		
	}
	
	//mapping lemburdanbonus
	private List<LemburdanbonusDTO> mappingLemburDanBonus(){
		List<Lemburdanbonus> listLemburDanBonus = lemburDanBonusRepository.findAll();
		List<LemburdanbonusDTO> listLemburDanBonusDTO = new ArrayList<LemburdanbonusDTO>();
		for(Lemburdanbonus lemburDanBonus : listLemburDanBonus) {
			LemburdanbonusDTO lemburdanbonusDTO = modelMapper.map(lemburDanBonus, LemburdanbonusDTO.class);
			listLemburDanBonusDTO.add(lemburdanbonusDTO);
		}
		return listLemburDanBonusDTO;
		
	}
	
	
	//mapping karyawan
	private List<KaryawanDTO> mappingKaryawan(){
		List<Karyawan> listKaryawan = karyawanRepository.findAll();
		List<KaryawanDTO> listKaryawanDTO = new ArrayList<KaryawanDTO>();
		for(Karyawan karyawan : listKaryawan) {
			KaryawanDTO karyawanDTO = modelMapper.map(karyawan, KaryawanDTO.class);
			listKaryawanDTO.add(karyawanDTO);
		}
		return listKaryawanDTO;
	}
	
	//mapping presentaseGaji
	private List<PresentaseGajiDTO> mappingPresentaseGaji(){
		List<PresentaseGaji> listPresentaseGaji = presentaseGajiRepository.findAll();
		List<PresentaseGajiDTO> listPresentaseGajiDTO =  new ArrayList<PresentaseGajiDTO>();
		for(PresentaseGaji presentaseGaji : listPresentaseGaji) {
			PresentaseGajiDTO presentaseGajiDTO = modelMapper.map(presentaseGaji, PresentaseGajiDTO.class);
			listPresentaseGajiDTO.add(presentaseGajiDTO);
		}
		return listPresentaseGajiDTO;
	}
	
	//mapping posisi
	private List<PosisiDTO> mappingPosisi(){
		List<Posisi> listPosisi = posisiRepository.findAll(Sort.by(Sort.Direction.ASC, "namaPosisi"));
		List<PosisiDTO> listPosisiDTO = new ArrayList<PosisiDTO>();
		for(Posisi posisi : listPosisi) {
			PosisiDTO posisiDTO = modelMapper.map(posisi, PosisiDTO.class);
			listPosisiDTO.add(posisiDTO);
		}
		return listPosisiDTO;
	}
	
	//mapping parameter
	private List<ParameterDTO> mappingParameter(){
		List<Parameter> listParameter = parameterRepository.findAll();
		List<ParameterDTO> listParameterDTO = new ArrayList<ParameterDTO>();
		for(Parameter parameter : listParameter) {
			ParameterDTO parameterDTO = modelMapper.map(parameter, ParameterDTO.class);
			listParameterDTO.add(parameterDTO);
		}
		return listParameterDTO;
	}
	
	//mapping tunjangan pegawai
	private List<TunjanganPegawaiDTO> mappingTunjanganPegawai(){
		List<TunjanganPegawai> listTunjanganPegawai = tunjanganPegawaiRepository.findAll();
		List<TunjanganPegawaiDTO> listTunjanganPegawaiDTO = new ArrayList<TunjanganPegawaiDTO>();
		for(TunjanganPegawai tunjanganPegawai :listTunjanganPegawai) {
			TunjanganPegawaiDTO tunjanganPegawaiDTO = modelMapper.map(tunjanganPegawai, TunjanganPegawaiDTO.class);
		    listTunjanganPegawaiDTO.add(tunjanganPegawaiDTO);
		}
		return listTunjanganPegawaiDTO;
	}
	//get besaran gaji
	private BigDecimal getBesaranGaji(KaryawanDTO karyawanDTO) {
		BigDecimal besaranGaji;
		besaranGaji = BigDecimal.ZERO;
		List<PresentaseGajiDTO> listPresentaseGajiDTO = mappingPresentaseGaji();
		
		List<PresentaseGajiDTO> listMasaKerja = new ArrayList<PresentaseGajiDTO>();
		//make list with specific posisi and tingkatan to get maximum experience and multiple for salary
		for(PresentaseGajiDTO presentaseGajiDTO : listPresentaseGajiDTO) {	
			if (karyawanDTO.getPosisi().getIdPosisi() == presentaseGajiDTO.getPosisi().getIdPosisi() && karyawanDTO.getTingkatan().getIdTingkatan() == presentaseGajiDTO.getTingkatan().getIdTingkatan()) {
				listMasaKerja.add(presentaseGajiDTO);
			}				
		}
		//get maximum experience from the new previous list
		PresentaseGajiDTO masaKerjaMaxDTO =  Collections.max(listMasaKerja, Comparator.comparing(s -> s.getMasaKerja()));
		//find the besaran gaji value
		for(PresentaseGajiDTO presentaseGajiDTO : listMasaKerja) {	
			if (karyawanDTO.getPosisi().getIdPosisi() == presentaseGajiDTO.getPosisi().getIdPosisi() && karyawanDTO.getTingkatan().getIdTingkatan() == presentaseGajiDTO.getTingkatan().getIdTingkatan() && karyawanDTO.getMasaKerja() == presentaseGajiDTO.getMasaKerja()) {
				besaranGaji = presentaseGajiDTO.getBesaranGaji();							
			}	
		}	
		if (karyawanDTO.getMasaKerja() > masaKerjaMaxDTO.getMasaKerja()) {
			besaranGaji = masaKerjaMaxDTO.getBesaranGaji();	
		}
		
		return besaranGaji;
	}
	
	
	//gaji pokok calculation
	private BigDecimal gajiPokokCalculation(BigDecimal besaranGaji, KaryawanDTO karyawanDTO){
		BigDecimal gajiPokok ;
		BigDecimal umkSalary = karyawanDTO.getPenempatan().getUmkPenempatan();
		gajiPokok = besaranGaji.multiply(umkSalary);
		return gajiPokok;
	}
	
	
	//calculatiion tunjangan keluarga
	private BigDecimal tunjanganKeluargaCalculation(Short statusMenikah, BigDecimal gajiPokok){	
		List<ParameterDTO> listParameterDTO = mappingParameter();
		Short isMarried = 1;
		BigDecimal multipleTunjanganKeluarga = BigDecimal.ZERO ;
		BigDecimal tunjanganKeluarga = BigDecimal.ZERO;
		for(ParameterDTO parameter : listParameterDTO) {
			multipleTunjanganKeluarga = parameter.getTKeluarga();
		}
		
		if(statusMenikah == isMarried) {
			tunjanganKeluarga = multipleTunjanganKeluarga.multiply(gajiPokok);
		}	
		else {
			tunjanganKeluarga = BigDecimal.ZERO;
		}

		return tunjanganKeluarga;	
	}
			
	//get tunjangan pegawai
	private BigDecimal getTunjanganPegawai(KaryawanDTO karyawanDTO){
		List<TunjanganPegawaiDTO> listTunjanganPegawaiDTO = mappingTunjanganPegawai();
		BigDecimal tunjanganPegawai = BigDecimal.ZERO ;
		
		for(TunjanganPegawaiDTO tunjanganPegawaiDTO : listTunjanganPegawaiDTO) {
			if(karyawanDTO.getPosisi().getIdPosisi() == tunjanganPegawaiDTO.getPosisi().getIdPosisi() && karyawanDTO.getTingkatan().getIdTingkatan() == tunjanganPegawaiDTO.getTingkatan().getIdTingkatan()) {
				tunjanganPegawai = tunjanganPegawaiDTO.getBesaranTujnaganPegawai();
			}			
		}
		
		return tunjanganPegawai;
	}
	//get tunjangan transport
		private BigDecimal getTunjanganTransport(KaryawanDTO karyawanDTO){
			List<ParameterDTO> listParameterDTO = mappingParameter();
			BigDecimal multipleTunjanganTransport = BigDecimal.ZERO;
			BigDecimal tunjanganTransport ;
			String penempatanTunjangan = "jakarta";
			String penempatan = karyawanDTO.getPenempatan().getKotaPenempatan();
			//get multiple tunjangan transport
			for(ParameterDTO parameterDTO: listParameterDTO) {
				multipleTunjanganTransport = parameterDTO.getTTransport();
			}
			//validating if contains specific city that get tunjanganTransport
			if (penempatan.toLowerCase().indexOf(penempatanTunjangan.toLowerCase()) != -1) {
				tunjanganTransport = multipleTunjanganTransport;
			}
			else {
				tunjanganTransport = BigDecimal.ZERO;
			}		
			return tunjanganTransport;
		}
	
	//bpjs calculation
	private BigDecimal bpjsCalculation(BigDecimal gajiPokok){
		List<ParameterDTO> listParameterDTO = mappingParameter();
		BigDecimal bpjs;
		BigDecimal bpjsMultiple = BigDecimal.ZERO;
		for(ParameterDTO parameterDTO : listParameterDTO) {
			bpjsMultiple = parameterDTO.getPBpjs();
		}
		//calculating
		bpjs = gajiPokok.multiply(bpjsMultiple);
		
		return bpjs;
 	}
	
	//calculation gaji kotor
	private BigDecimal gajiKotorCalculation(BigDecimal gajiPokok, BigDecimal tunjanganKeluarga, BigDecimal tunjanganPegawai, BigDecimal tunjanganTransport){
		BigDecimal gajiKotor;	
		//calculating
		gajiKotor = gajiPokok.add(tunjanganPegawai).add(tunjanganKeluarga).add(tunjanganTransport);	
		
		return gajiKotor;
	}
	
	//calculation gaji bersih
	private BigDecimal gajiBersihCalculation(BigDecimal gajiKotor, BigDecimal pphPerBulan, BigDecimal bpjsDeduction){
		BigDecimal gajiBersih;
		//calculating
		gajiBersih = gajiKotor.subtract(bpjsDeduction).subtract(pphPerBulan);
		
		return gajiBersih;
		}		
	
	
	
	//set Take home pay
	private BigDecimal takeHomePayCalculation(BigDecimal gajiBersih,  BigDecimal uangLembur, BigDecimal uangBonus) {
		BigDecimal takeHomePay;
		//calculating
		takeHomePay = gajiBersih.add(uangLembur).add(uangBonus).add(uangBonus);
		return takeHomePay;
	}
	//bonus salary calculation
	private BigDecimal bonusCalculation(KaryawanDTO karyawan, Date tanggalGaji) {
		List<LemburdanbonusDTO> listLemburDanBonusDTO = mappingLemburDanBonus();
		List<ParameterDTO> listParameterDTO = mappingParameter();
		List<PosisiDTO> listPosisiDTO = mappingPosisi();
		BigDecimal multipleBonus = BigDecimal.ZERO;
		BigDecimal bonusSalary = BigDecimal.ZERO;
		BigDecimal maxBonus = BigDecimal.ZERO;
		int batasanBonus = 0;
		
		for(ParameterDTO parameterDTO : listParameterDTO) {
			if(karyawan.getPosisi().getNamaPosisi().equalsIgnoreCase(listPosisiDTO.get(1).getNamaPosisi()) ) {
				multipleBonus = parameterDTO.getBonusPg();
				batasanBonus = parameterDTO.getBatasanBonusPg();
				
			}
			else if(karyawan.getPosisi().getNamaPosisi().equalsIgnoreCase(listPosisiDTO.get(2).getNamaPosisi())){
				multipleBonus = parameterDTO.getBonusTw();
				batasanBonus = parameterDTO.getBatasanBonusTw();
				
			}
			else if(karyawan.getPosisi().getNamaPosisi().equalsIgnoreCase(listPosisiDTO.get(3).getNamaPosisi())) {
				multipleBonus = parameterDTO.getBonusTs();
				batasanBonus = parameterDTO.getBatasanBonusTs();
				
			}
			maxBonus = parameterDTO.getMaxBonus();
			
			
		}
		for(LemburdanbonusDTO lemburDanBonusDTO : listLemburDanBonusDTO) {
			if(lemburDanBonusDTO.getKaryawan().getIdKaryawan() == karyawan.getIdKaryawan() && tanggalGaji.getYear() == lemburDanBonusDTO.getTanggal().getYear() && tanggalGaji.getMonth() == lemburDanBonusDTO.getTanggal().getMonth()) {
				double variableBonus = lemburDanBonusDTO.getVariableBonus();
				double batasanBonusDouble = batasanBonus;
				double besaranBonus = variableBonus / batasanBonusDouble;
				int bulatkanBesaranBonus = (int) besaranBonus;
				bonusSalary = multipleBonus.multiply(BigDecimal.valueOf(bulatkanBesaranBonus));
				if (bonusSalary.compareTo(maxBonus) > 0) {
					bonusSalary = maxBonus;
				}
			}
			
		}
		System.out.println("bonus salary : " + bonusSalary);
		return bonusSalary;
	}
	//lembur salary calculation
	private BigDecimal lemburCalculation(KaryawanDTO karyawan, BigDecimal gajiBersih, Date tanggalGaji) {
		List<LemburdanbonusDTO> listLemburDanBonusDTO = mappingLemburDanBonus();
		List<ParameterDTO> listParameterDTO = mappingParameter();
		BigDecimal multipleLembur,  totalLemburCalculation;
		multipleLembur = BigDecimal.ZERO;
		totalLemburCalculation = BigDecimal.ZERO;
		int totalLemburHours ;
		for(ParameterDTO parameterDTO : listParameterDTO) {
			multipleLembur = parameterDTO.getLembur();
		}
		for(LemburdanbonusDTO lemburDanBonusDTO : listLemburDanBonusDTO) {
			if(lemburDanBonusDTO.getKaryawan().getIdKaryawan() == karyawan.getIdKaryawan() && tanggalGaji.getYear() == lemburDanBonusDTO.getTanggal().getYear() && tanggalGaji.getMonth() == lemburDanBonusDTO.getTanggal().getMonth()) {			
				
				totalLemburHours = lemburDanBonusDTO.getJumlahLembur();
				totalLemburCalculation = multipleLembur.multiply(BigDecimal.valueOf(totalLemburHours).multiply(gajiBersih));
			}
			
			
		}
		return totalLemburCalculation;
	}
	//get lama lembur
	private int getLamaLembur(KaryawanDTO karyawanDTO) {
		List<LemburdanbonusDTO> listLemburDanBonusDTO = mappingLemburDanBonus();
		int lamaLembur = 0;
		for(LemburdanbonusDTO lemburDanBonusDTO : listLemburDanBonusDTO) {
			if(lemburDanBonusDTO.getKaryawan().getIdKaryawan() == karyawanDTO.getIdKaryawan()) {
				lamaLembur = lemburDanBonusDTO.getJumlahLembur();
			}
		}
		return lamaLembur;
	}
	//get variable bonus
	private int getVariableBonus(KaryawanDTO karyawanDTO) {
		List<LemburdanbonusDTO> listLemburDanBonusDTO = mappingLemburDanBonus();
		int variableBonus = 0;
		for(LemburdanbonusDTO lemburDanBonusDTO : listLemburDanBonusDTO) {
			if(lemburDanBonusDTO.getKaryawan().getIdKaryawan() == karyawanDTO.getIdKaryawan()) {
				variableBonus = lemburDanBonusDTO.getVariableBonus();
			}
		}
		return variableBonus;
	}
	
	
	
}
