package com.office.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.office.employee.dtomodels.AgamaDTO;
import com.office.employee.models.Agama;
import com.office.employee.repositories.AgamaRepository;

@RestController
@RequestMapping("api/agama")
public class AgamaController {
	@Autowired AgamaRepository agamaRepository;
	ModelMapper modelMapper = new ModelMapper();
	
	@PostMapping("/create")
	public Map<String, Object> inputDataAgama(@Valid @RequestBody AgamaDTO agamaDTO){
		Map<String, Object> result = new HashMap<String, Object>();
		Agama agama = modelMapper.map(agamaDTO, Agama.class);
		agamaRepository.save(agama);
		result.put("Status", 200);
		result.put("Message", "Create data agama succesful");
		result.put("Data", agama);
		return result;
	}
	
	@GetMapping("/readAll")
	public Map<String, Object> getAllAgama(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<Agama> listAgama = agamaRepository.findAll();
		List<AgamaDTO> listAgamaDTO = new ArrayList<AgamaDTO>();
		for(Agama agama : listAgama) {
			AgamaDTO agamaDTO = modelMapper.map(agama, AgamaDTO.class);
			listAgamaDTO.add(agamaDTO);
		}
		result.put("Status", 200);
		result.put("Message", "Read all data agama succesful");
		result.put("Data", listAgamaDTO);
		return result;
	}
	
	@GetMapping("/read/{id}") 
	public Map<String, Object> getAgamaById(@PathVariable(value = "id") Long idAgama){
		Map<String, Object> result = new HashMap<String, Object>();
		Agama agama = agamaRepository.findById(idAgama).get();
		AgamaDTO agamaDTO =  modelMapper.map(agama, AgamaDTO.class);
		result.put("Status", 200);
		result.put("Message", "Read data agama by id succesful");
		result.put("Data", agamaDTO);
		return result;
	}
	
	@PutMapping("/update/{id}")
	public Map<String, Object> updateAgama(@PathVariable(value = "id") Long idAgama, @Valid @RequestBody AgamaDTO agamaDTO){
		Agama agama = agamaRepository.findById(idAgama).get();
		agama = modelMapper.map(agamaDTO, Agama.class);
		agama.setIdAgama(idAgama);
		agamaRepository.save(agama);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Update data agama by id succesful");
		result.put("Data", agama);
		return result;
	}
	
	@DeleteMapping("delete/{id}")
	public Map<String, Object> deleteAgama(@PathVariable(value = "id")Long idAgama){
		Agama agama = agamaRepository.findById(idAgama).get();
		agamaRepository.delete(agama);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Delete data agama succesful");
		result.put("Data", ResponseEntity.ok().build());
		return result;
	}
	
}
