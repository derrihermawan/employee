package com.office.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.office.employee.dtomodels.PosisiDTO;
import com.office.employee.models.Posisi;
import com.office.employee.repositories.PosisiRepository;

@RestController
@RequestMapping("api/posisi")
public class PosisiController {
	@Autowired 
	PosisiRepository posisiRepository;
	ModelMapper modelMapper = new ModelMapper();
	
	@PostMapping("/create")
	public Map<String, Object> inputDataPosisi(@Valid @RequestBody PosisiDTO posisiDTO){
		Posisi posisi = modelMapper.map(posisiDTO, Posisi.class);
		posisiRepository.save(posisi);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Create data posisi succesful");
		result.put("Data", posisi);
		return result;
	}
	
	@GetMapping("/readAll")
	public Map<String, Object> getAllPosisi (){
		List<Posisi> listPosisi = posisiRepository.findAll();
		List<PosisiDTO> listPosisiDTO = new ArrayList<PosisiDTO>();
		for(Posisi posisi : listPosisi) {
			PosisiDTO posisiDTO = modelMapper.map(posisi, PosisiDTO.class);
			listPosisiDTO.add(posisiDTO);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Read all data posisi succesful");
		result.put("Data", listPosisiDTO);
		return result;
	}
	
	@GetMapping("/read/{id}")
	public Map<String, Object> getPosisiById(@PathVariable(value = "id")Long idPosisi){
		Posisi posisi = posisiRepository.findById(idPosisi).get();
		PosisiDTO posisiDTO = modelMapper.map(posisi, PosisiDTO.class);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Read data posisi by id succesful");
		result.put("Data", posisiDTO);
		return result;
	}
	
	@PutMapping("/update/{id}")
	public Map<String, Object> updateDataPosisi(@PathVariable(value = "id") Long idPosisi, @Valid @RequestBody PosisiDTO posisiDTO){
		Posisi posisi = modelMapper.map(posisiDTO, Posisi.class);
		posisi.setIdPosisi(idPosisi);
		posisiRepository.save(posisi);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Update data posisi succesful");
		result.put("Data", posisi);
		return result;
	}
	
	@DeleteMapping("/delete/{id}")
	public Map<String, Object> deleteDataPosisi(@PathVariable(value = "id")Long idPosisi){
		Posisi posisi = posisiRepository.findById(idPosisi).get();
		posisiRepository.delete(posisi);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Delete data posisi succesful");
		result.put("Data", ResponseEntity.ok().build());
		return result;
	}
}
