package com.office.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.office.employee.dtomodels.ParameterDTO;
import com.office.employee.models.Parameter;
import com.office.employee.repositories.ParameterRepository;

@RestController
@RequestMapping("/api/parameter")
public class ParameterController {
	@Autowired 
	ParameterRepository parameterRepository;
	ModelMapper modelMapper = new ModelMapper();
	
	@PostMapping("/create")
	public Map<String, Object> inputDataParameter(@Valid @RequestBody ParameterDTO parameterDTO){
		Parameter parameter = modelMapper.map(parameterDTO, Parameter.class);
		parameterRepository.save(parameter);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Create data parameter succesful");
		result.put("Data", parameter);
		return result;
	}
	
	@GetMapping("/readAll")
	public Map<String, Object> getAllParameter (){
		List<Parameter> listParameter = parameterRepository.findAll();
		List<ParameterDTO> listParameterDTO = new ArrayList<ParameterDTO>();
		for(Parameter parameter : listParameter) {
			ParameterDTO parameterDTO = modelMapper.map(parameter, ParameterDTO.class);
			listParameterDTO.add(parameterDTO);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Read all data parameter succesful");
		result.put("Data", listParameterDTO);
		return result;
	}
	
	@GetMapping("/read/{id}")
	public Map<String, Object> getParameterById(@PathVariable(value = "id")Long idParameter){
		Parameter parameter = parameterRepository.findById(idParameter).get();
		ParameterDTO parameterDTO = modelMapper.map(parameter, ParameterDTO.class);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Read data parameter by id succesful");
		result.put("Data", parameterDTO);
		return result;
	}
	
	@PutMapping("/update/{id}")
	public Map<String, Object> updateDataParameter(@PathVariable(value = "id") Long idParameter, @Valid @RequestBody ParameterDTO parameterDTO){
		Parameter parameter = modelMapper.map(parameterDTO, Parameter.class);
		parameter.setIdParam(idParameter);
		parameterRepository.save(parameter);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Update data parameter succesful");
		result.put("Data", parameter);
		return result;
	}
	
	@DeleteMapping("/delete/{id}")
	public Map<String, Object> deleteDataParameter(@PathVariable(value = "id")Long idParameter){
		Parameter parameter = parameterRepository.findById(idParameter).get();
		parameterRepository.delete(parameter);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Delete data parameter succesful");
		result.put("Data", ResponseEntity.ok().build());
		return result;
	}
}
