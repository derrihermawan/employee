package com.office.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.office.employee.dtomodels.PresentaseGajiDTO;
import com.office.employee.models.PresentaseGaji;
import com.office.employee.repositories.PresentaseGajiRepository;

@RestController
@RequestMapping("/api/presentasegaji")
public class PresentaseGajiController {
	@Autowired
	PresentaseGajiRepository presentaseGajiRepository;
	ModelMapper modelMapper = new ModelMapper();
	
	@PostMapping("/create")
	public Map<String, Object> inputDataPresentaseGaji(@Valid @RequestBody PresentaseGajiDTO presentaseGajiDTO){
		PresentaseGaji presentaseGaji = modelMapper.map(presentaseGajiDTO, PresentaseGaji.class);
		presentaseGajiRepository.save(presentaseGaji);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Create data presentaseGaji succesful");
		result.put("Data", presentaseGaji);
		return result;
	}
	
	@GetMapping("/readAll")
	public Map<String, Object> getAllPresentaseGaji (){
		List<PresentaseGaji> listPresentaseGaji = presentaseGajiRepository.findAll();
		List<PresentaseGajiDTO> listPresentaseGajiDTO = new ArrayList<PresentaseGajiDTO>();
		for(PresentaseGaji presentaseGaji : listPresentaseGaji) {
			PresentaseGajiDTO presentaseGajiDTO = modelMapper.map(presentaseGaji, PresentaseGajiDTO.class);
			listPresentaseGajiDTO.add(presentaseGajiDTO);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Read all data presentaseGaji succesful");
		result.put("Data", listPresentaseGajiDTO);
		return result;
	}
	
	@GetMapping("/read/{id}")
	public Map<String, Object> getPresentaseGajiById(@PathVariable(value = "id")Long idPresentaseGaji){
		PresentaseGaji presentaseGaji = presentaseGajiRepository.findById(idPresentaseGaji).get();
		PresentaseGajiDTO presentaseGajiDTO = modelMapper.map(presentaseGaji, PresentaseGajiDTO.class);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Read data presentaseGaji by id succesful");
		result.put("Data", presentaseGajiDTO);
		return result;
	}
	
	@PutMapping("/update/{id}")
	public Map<String, Object> updateDataPresentaseGaji(@PathVariable(value = "id") Long idPresentaseGaji, @Valid @RequestBody PresentaseGajiDTO presentaseGajiDTO){
		PresentaseGaji presentaseGaji = modelMapper.map(presentaseGajiDTO, PresentaseGaji.class);
		presentaseGaji.setIdPresentaseGaji(idPresentaseGaji);
		presentaseGajiRepository.save(presentaseGaji);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Update data presentaseGaji succesful");
		result.put("Data", presentaseGaji);
		return result;
	}
	
	@DeleteMapping("/delete/{id}")
	public Map<String, Object> deleteDataPresentaseGaji(@PathVariable(value = "id")Long idPresentaseGaji){
		PresentaseGaji presentaseGaji = presentaseGajiRepository.findById(idPresentaseGaji).get();
		presentaseGajiRepository.delete(presentaseGaji);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Delete data presentaseGaji succesful");
		result.put("Data", ResponseEntity.ok().build());
		return result;
	}
}
