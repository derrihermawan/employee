package com.office.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.office.employee.dtomodels.LemburdanbonusDTO;
import com.office.employee.models.Lemburdanbonus;
import com.office.employee.repositories.LemburDanBonusRepository;

@RestController
@RequestMapping("/api/lemburdanbonus")
public class LemburDanBonusController {
	@Autowired
	LemburDanBonusRepository lemburDanBonusRepository;
	ModelMapper modelMapper = new ModelMapper();
	//create data lembur dan bonus
	@PostMapping("/create")
	public Map<String, Object> inputLemburDanBonus(@Valid @RequestBody LemburdanbonusDTO lemburDanBonusDTO){
		Map<String, Object> result = new HashMap<String, Object>();
		boolean isExist = false;
		
		List<LemburdanbonusDTO> listLemburDanBonusDTO = mappingLemburDanBonus();
		for(LemburdanbonusDTO validateLemburDanBonusDTO : listLemburDanBonusDTO) {
			if(validateLemburDanBonusDTO.getKaryawan().getIdKaryawan() == lemburDanBonusDTO.getKaryawan().getIdKaryawan() && validateLemburDanBonusDTO.getTanggal().getYear() == lemburDanBonusDTO.getTanggal().getYear() && validateLemburDanBonusDTO.getTanggal().getMonth() == lemburDanBonusDTO.getTanggal().getMonth()) {
				result.put("Status", "Data is not created..");
				result.put("Message", "Data bonus dan gaji with that karyawan and tanggal was exist, please input another data!");
				isExist = true;
			}
		}
		if (!isExist) {
			Lemburdanbonus lemburDanBonus = modelMapper.map(lemburDanBonusDTO, Lemburdanbonus.class);
			lemburDanBonusRepository.save(lemburDanBonus);
			result.put("Status", 200);
			result.put("Message", "Create data bonus dan gaji succesfull");
			result.put("Data", lemburDanBonus);
		}
		
		return result;
	}
	
	//read all lembur dan bonus
	@GetMapping("/readAll")
	public Map<String, Object> getAllLemburDanBonus(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<LemburdanbonusDTO> listLemburDanBonusDTO = mappingLemburDanBonus();
		
		result.put("Status", 200);
		result.put("Message", "Read all data lembur dan bonus succesfull");
		result.put("Data", listLemburDanBonusDTO);
		return result;
		
	}
	
	//read by id
	@GetMapping("/read/{id}")
	public Map<String, Object> getLemburDanBonusByID(@PathVariable(value = "id") Long idLemburDanBonus){
		Map<String, Object> result = new HashMap<String, Object>();
		Lemburdanbonus lemburDanBonus = lemburDanBonusRepository.findById(idLemburDanBonus).get();
		LemburdanbonusDTO lemburDanBonusDTO = modelMapper.map(lemburDanBonus, LemburdanbonusDTO.class);
		result.put("Status", 200);
		result.put("Message", "Read data lembur dan bonus by id succesfull");
		result.put("Data", lemburDanBonusDTO);
		return result;
	}
	
	//update lembur dan bonus
	@PutMapping("update/{id}")
	public Map<String, Object> updateLemburDanBonus(@PathVariable(value = "id") Long idLemburDanBonus, @Valid @RequestBody LemburdanbonusDTO lemburDanBonusDTO){
		Map<String, Object> result = new HashMap<String, Object>();
		Lemburdanbonus lemburDanBonus = lemburDanBonusRepository.findById(idLemburDanBonus).get();
		lemburDanBonus = modelMapper.map(lemburDanBonusDTO, Lemburdanbonus.class);
		lemburDanBonus.setIdLemburdanbonus(idLemburDanBonus);
		lemburDanBonusRepository.save(lemburDanBonus);
		result.put("Status", 200);
		result.put("Message", "Update data lembur dan bonus succesfull");
		result.put("Data", lemburDanBonus);
		return result;
		
	}
	
	//delete data lembur dan bonus
	@DeleteMapping("delete/{id}")
	public Map<String, Object> deleteLemburDanBonus(@PathVariable(value = "id") Long idLemburDanBonus){
		Map<String, Object> result = new HashMap<String, Object>();
		Lemburdanbonus lemburDanBonus = lemburDanBonusRepository.findById(idLemburDanBonus).get();
		lemburDanBonusRepository.delete(lemburDanBonus);
		result.put("Status", 200);
		result.put("Message", "Delete data lembur dan bonus succesfull");
		result.put("Data", ResponseEntity.ok().build());
		return result;
	}
	
	//mapping lembur dan bonus
	private List<LemburdanbonusDTO> mappingLemburDanBonus(){
		List<Lemburdanbonus> listLemburDanBonus = lemburDanBonusRepository.findAll();
		List<LemburdanbonusDTO> listLemburDanBonusDTO = new ArrayList<LemburdanbonusDTO>();
		
		for(Lemburdanbonus lemburDanBonus : listLemburDanBonus) {
			LemburdanbonusDTO lemburDanBonusDTO = modelMapper.map(lemburDanBonus, LemburdanbonusDTO.class);
			listLemburDanBonusDTO.add(lemburDanBonusDTO);
		}
		return listLemburDanBonusDTO;
	}
	
}
