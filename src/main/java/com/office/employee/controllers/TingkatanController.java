package com.office.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.office.employee.dtomodels.TingkatanDTO;
import com.office.employee.models.Tingkatan;
import com.office.employee.repositories.TingkatanRepository;

@RestController
@RequestMapping("/api/tingkatan")
public class TingkatanController {
	@Autowired
	TingkatanRepository tingkatanRepository;
	ModelMapper modelMapper = new ModelMapper();
	
	@PostMapping("/create")
	public Map<String, Object> inputDataTingkatan(@Valid @RequestBody TingkatanDTO tingkatanDTO){
		Tingkatan tingkatan = modelMapper.map(tingkatanDTO, Tingkatan.class);
		tingkatanRepository.save(tingkatan);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Create data tingkatan succesful");
		result.put("Data", tingkatan);
		return result;
	}
	
	@GetMapping("/readAll")
	public Map<String, Object> getAllTingkatan (){
		List<Tingkatan> listTingkatan = tingkatanRepository.findAll();
		List<TingkatanDTO> listTingkatanDTO = new ArrayList<TingkatanDTO>();
		for(Tingkatan tingkatan : listTingkatan) {
			TingkatanDTO tingkatanDTO = modelMapper.map(tingkatan, TingkatanDTO.class);
			listTingkatanDTO.add(tingkatanDTO);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Read all data tingkatan succesful");
		result.put("Data", listTingkatanDTO);
		return result;
	}
	
	@GetMapping("/read/{id}")
	public Map<String, Object> getTingkatanById(@PathVariable(value = "id")Long idTingkatan){
		Tingkatan tingkatan = tingkatanRepository.findById(idTingkatan).get();
		TingkatanDTO tingkatanDTO = modelMapper.map(tingkatan, TingkatanDTO.class);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Read data tingkatan by id succesful");
		result.put("Data", tingkatanDTO);
		return result;
	}
	
	@PutMapping("/update/{id}")
	public Map<String, Object> updateDataTingkatan(@PathVariable(value = "id") Long idTingkatan, @Valid @RequestBody TingkatanDTO tingkatanDTO){
		Tingkatan tingkatan = modelMapper.map(tingkatanDTO, Tingkatan.class);
		tingkatan.setIdTingkatan(idTingkatan);
		tingkatanRepository.save(tingkatan);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Update data tingkatan succesful");
		result.put("Data", tingkatan);
		return result;
	}
	
	@DeleteMapping("/delete/{id}")
	public Map<String, Object> deleteDataTingkatan(@PathVariable(value = "id")Long idTingkatan){
		Tingkatan tingkatan = tingkatanRepository.findById(idTingkatan).get();
		tingkatanRepository.delete(tingkatan);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Delete data tingkatan succesful");
		result.put("Data", ResponseEntity.ok().build());
		return result;
	}
}
