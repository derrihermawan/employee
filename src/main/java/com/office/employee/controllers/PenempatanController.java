package com.office.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.office.employee.dtomodels.PenempatanDTO;
import com.office.employee.models.Penempatan;
import com.office.employee.repositories.PenempatanRepository;

@RestController
@RequestMapping("/api/penempatan")
public class PenempatanController {
	@Autowired PenempatanRepository penempatanRepository;
	ModelMapper modelMapper = new ModelMapper();
	
	@PostMapping("/create")
	public Map<String, Object> inputDataPenempatan(@Valid @RequestBody PenempatanDTO penempatanDTO){
		Penempatan penempatan = modelMapper.map(penempatanDTO, Penempatan.class);
		penempatanRepository.save(penempatan);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Create data penempatan succesful");
		result.put("Data", penempatan);
		return result;
	}
	
	@GetMapping("/readAll")
	public Map<String, Object> getAllPenempatan (){
		List<Penempatan> listPenempatan = penempatanRepository.findAll();
		List<PenempatanDTO> listPenempatanDTO = new ArrayList<PenempatanDTO>();
		for(Penempatan penempatan : listPenempatan) {
			PenempatanDTO penempatanDTO = modelMapper.map(penempatan, PenempatanDTO.class);
			listPenempatanDTO.add(penempatanDTO);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Read all data penempatan succesful");
		result.put("Data", listPenempatanDTO);
		return result;
	}
	
	@GetMapping("/read/{id}")
	public Map<String, Object> getPenempatanById(@PathVariable(value = "id")Long idPenempatan){
		Penempatan penempatan = penempatanRepository.findById(idPenempatan).get();
		PenempatanDTO penempatanDTO = modelMapper.map(penempatan, PenempatanDTO.class);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Read data penempatan by id succesful");
		result.put("Data", penempatanDTO);
		return result;
	}
	
	@PutMapping("/update/{id}")
	public Map<String, Object> updateDataPenempatan(@PathVariable(value = "id") Long idPenempatan, @Valid @RequestBody PenempatanDTO penempatanDTO){
		Penempatan penempatan = modelMapper.map(penempatanDTO, Penempatan.class);
		penempatan.setIdPenempatan(idPenempatan);
		penempatanRepository.save(penempatan);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Update data penempatan succesful");
		result.put("Data", penempatan);
		return result;
	}
	
	@DeleteMapping("/delete/{id}")
	public Map<String, Object> deleteDataPenempatan(@PathVariable(value = "id")Long idPenempatan){
		Penempatan penempatan = penempatanRepository.findById(idPenempatan).get();
		penempatanRepository.delete(penempatan);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Delete data penempatan succesful");
		result.put("Data", ResponseEntity.ok().build());
		return result;
	}
}
