package com.office.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.office.employee.dtomodels.KaryawanDTO;
import com.office.employee.models.Karyawan;
import com.office.employee.repositories.KaryawanRepository;

@RestController
@RequestMapping("/api/karyawan")
public class KaryawanController {
	@Autowired KaryawanRepository karyawanRepository;
	ModelMapper modelMapper = new ModelMapper();
	
	@PostMapping("/create")
	public Map<String, Object> inputDataKaryawan(@Valid @RequestBody KaryawanDTO karyawanDTO){
		Map<String, Object> result = new HashMap<String, Object>();
		Karyawan karyawan = modelMapper.map(karyawanDTO, Karyawan.class);
		karyawanRepository.save(karyawan);
		result.put("Status", 200);
		result.put("Message", "Input data karyawan succesful");
		result.put("Data", karyawan);
		return result;
		
	}
	
	@GetMapping("/readAll")
	public Map<String, Object> getAllKaryawan(){
		List<Karyawan> listKaryawan = karyawanRepository.findAll();
		List<KaryawanDTO> listKaryawanDTO = new ArrayList<KaryawanDTO>();
		for(Karyawan karyawan : listKaryawan) {
			KaryawanDTO karyawanDTO = modelMapper.map(karyawan, KaryawanDTO.class);
			listKaryawanDTO.add(karyawanDTO);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Get all data karyawan succesful");
		result.put("Data", listKaryawanDTO);
		return result;
	}
	
	@GetMapping("/read/{id}")
	public Map<String, Object> getKaryawanById(@PathVariable(value = "id")Long idKaryawan){
		Karyawan karyawan = karyawanRepository.findById(idKaryawan).get();
		KaryawanDTO karyawanDTO = modelMapper.map(karyawan, KaryawanDTO.class);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Get data karyawan by id succesful");
		result.put("Data", karyawanDTO);
		return result;
	}
	
	@PutMapping("/update/{id}")
	public Map<String, Object> updateDataKaryawan(@PathVariable(value = "id") Long idKaryawan, @Valid @RequestBody KaryawanDTO karyawanDTO){
		Karyawan karyawan = modelMapper.map(karyawanDTO, Karyawan.class);
		karyawan.setIdKaryawan(idKaryawan);
		karyawanRepository.save(karyawan);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "update data karyawan succesful");
		result.put("Data", karyawan);
		return result;
	}
	
	@DeleteMapping("/delete/{id}")
	public Map<String, Object> deleteDataKaryawan(@PathVariable(value = "id") Long idKaryawan){
		Karyawan karyawan = karyawanRepository.findById(idKaryawan).get();
		karyawanRepository.delete(karyawan);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Message", "Delete data karyawan succesful");
		result.put("Data", ResponseEntity.ok().build());
		return result;
	}
}
