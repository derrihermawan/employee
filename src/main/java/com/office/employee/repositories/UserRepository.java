package com.office.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.office.employee.models.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

}
