package com.office.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.office.employee.models.PresentaseGaji;

@Repository
public interface PresentaseGajiRepository extends JpaRepository<PresentaseGaji, Long> {

}
