package com.office.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.office.employee.models.Tingkatan;

@Repository
public interface TingkatanRepository extends JpaRepository<Tingkatan, Long>{

}
