package com.office.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.office.employee.models.Karyawan;
@Repository
public interface KaryawanRepository extends JpaRepository<Karyawan, Long>{

}
