package com.office.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.office.employee.models.Posisi;

@Repository
public interface PosisiRepository extends JpaRepository<Posisi, Long>{

}
