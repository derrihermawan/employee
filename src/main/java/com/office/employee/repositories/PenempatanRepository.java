package com.office.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.office.employee.models.Penempatan;

@Repository
public interface PenempatanRepository extends JpaRepository<Penempatan, Long> {

}
