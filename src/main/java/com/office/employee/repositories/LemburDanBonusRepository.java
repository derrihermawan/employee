package com.office.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.office.employee.models.Lemburdanbonus;

@Repository
public interface LemburDanBonusRepository extends JpaRepository<Lemburdanbonus, Long> {

}
